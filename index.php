<?php
//including the database connection file
include_once("classes/Crud.php");

$crud = new Crud();

//fetching data in descending order (lastest entry first)
$query = "SELECT * FROM products ORDER BY id DESC";
$result = $crud->getData($query);
// echo '<pre>'; print_r($result); exit;


include "view/layout/header.php";
?>
	<div class="container">
		<div class="d-flex pt-3">
			<h3 class="text-uppercase">product list</h3>
			<button type="submit" form="checkbox1" name="MASS DELETE" id="delete-product-btn" value="MASS DELETE" class="btn btn-danger float-right ml-auto">MASS DELETE</button>
			<button onclick="window.location.href='create.php';" style="margin-left: 1em;" class="btn btn-info float-right" value="ADD">ADD</button> 
		</div>

		<!-- onclick="location.href='create.php';" -->
		<hr>
		<form action="delete.php" method="POST" id="checkbox1">
		<div class="row">
			<?php 
				foreach ($result as $key => $res) {	?>	
				<div class='col-md-3 mt-3'>
					<div class='card text-center bg-light'>
						<div class='card-body'>
						
								<input type="checkbox" name="products1[]" value="<?php echo $res['id'];?>" class="float-left delete-checkbox">
							
								<p class='card-text'> <?php echo $res['sku']?> </p>
								<p> <?php echo $res['name']; ?> </p>
								<p> $ <?php echo $res['price']; ?> </p>

								<?php
								if ($res['size'] !== "") { ?>
									<p> Size: <?php echo $res['size']?> MB </p> <?php
								
								} else if ($res['weight'] !== "") { ?>
									<p> Weight: <?php echo $res['weight']?> KG </p> <?php
								
								} else { ?>
									<p> Dimension: <?php echo $res['dimension']?> </p> <?php
								} ?>
						</div>
					</div>				
				</div>	<?php
			}
			?>
			
		</div>
		</form>
	</div>

<?php
	include_once("view/layout/footer.php");
?>

<script src="script.js"></script>

</body>
</html>
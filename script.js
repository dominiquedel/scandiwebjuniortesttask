let button_check = document.getElementById("delete-product-btn");
	let box_check = document.querySelectorAll(".delete-checkbox");

	let converted = Array.from(box_check);
	let statusof = false;

	let arraytopass = [];
	let converted1 = Object.assign({}, arraytopass);

	let delete_checker = ""
	
	converted.forEach((key) => {
		key.addEventListener('change', ()=> {
			
			if(key.checked) {
				let takeval = key.getAttribute('value');
				
				if(arraytopass.indexOf(takeval) == -1) {
					arraytopass.push(takeval);
				}				
			} else {
				let takeval = key.getAttribute('value');
				let assigner = arraytopass.indexOf(takeval);
				arraytopass.splice(assigner, 1);			
			}
		})
	})

    const checker = () => {

		converted.forEach((key)=> {
			if(key.checked == true) {
				return statusof = true;
			} else {
				return event.preventDefault();
			}
		})
	}

	button_check.addEventListener('click', checker);
	document.getElementById("checkbox1").addEventListener("submit", send_data);

	function send_data (ev) {
		ev.preventDefault();

		let myform = ev.target;
		let fd = new FormData(myform)
		let searchparams = new URLSearchParams();
		
		for (const pair of fd ) {
			searchparams.append(pair[0], pair[1])
		}

	    fetch('delete.php', {
			method: 'post',
			body: searchparams
		}).then(function (response) {
			return response.text();
		}).then(function (text) {
			console.log(text);
		}).catch(function (error) {
			console.error(error);
		})

	    converted.forEach((key)=> {
			if(key.checked == true) {
				let the_parent = key.parentElement.parentElement.parentElement;
				the_parent.remove();
			}
		})
		
	}
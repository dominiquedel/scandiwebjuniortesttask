<?php
include "view/layout/header.php";
?>

<div class="container">
    <div class="d-flex pt-3">
		<h3 class="text-uppercase">product add</h3>
	</div>
    <hr>
    
	<div id="msg"></div>
	<div class="row">
        <div class="col-md-12">
            <!--<form action="add.php" method="post" name="form1" onsubmit = "return(validate());">-->
            <form action="add.php" method="post" name="form1" id="product_form" onsubmit="validate();">
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" name="sku" class="form-control w-50" id="sku">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control w-50" id="name">
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" name="price" class="form-control w-50" id="price">
                </div>

                <!-- type switcher selector -->
                <div class="form-group">
                    <label for="typeSwitcher">Type switcher</label>
                    <select class="form-control w-50" id="productType" name="type_switcher1">
                        <option selected disabled value=''></option>
                        <option value="DVD">DVD-disc</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </div>

                <!-- types -->

                <!-- DVD-disc -->
                <div class="form-group selectable" name="disc" id="DVD" style="display: none;">
                    <label for="size">Size</label>
                    <input type="number" name="size" class="form-control w-50" id="size">
                    <small>Please provide size.</small>
                </div>

                <!-- Book -->
                <div class="form-group selectable" name="book" id="book" style="display: none;">
                    <label for="weight">Weight</label>
                    <input type="number" name="weight" class="form-control w-50" id="weight">
                    <small>Please, provide weight.</small>
                </div>

                <!-- Furniture -->
                <div class="form-group selectable" name="furniture" id="furniture" style="display: none;">
                    
                    <label for="height">Height</label>
                    <input type="number" name="height" class="form-control w-50" id="height">

                    <label for="width">Width</label>
                    <input type="number" name="width" class="form-control w-50" id="width">

                    <label for="length">Length</label>
                    <input type="number" name="length" class="form-control w-50" id="length">

                    <small>Please, provide dimensions.</small>
                </div>

                <!-- Buttons: Save and Cancel -->
                <div class="form-group">
                    <button type="submit" class="btn btn-success"  name="Submit" value="submit" form="product_form" >Save</button>
                    <button type="button" onclick="window.location.href='index.php';" class="btn btn-danger ml-auto">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
	include "view/layout/footer.php";
?>

<script>
    $("#productType").on("change", function() {
        $(".selectable").hide();
        const a = $("#" + $(this).val()).show();
        console.log(a);
    })

	function validate() {
	    
	    const furni_display_check = document.getElementById('furniture').style.display;
        const book_display_check = document.getElementById('book').style.display;
        const disc_display_check = document.getElementById('DVD').style.display;

        const furni_input_check_ht = document.getElementById('height').value;
        const furni_input_check_wd = document.getElementById('width').value;
        const furni_input_check_lt = document.getElementById('length').value;

        const book_input_check = document.getElementById('weight').value;
        const disc_input_check = document.getElementById('size').value;

        if (document.form1.sku.value == '') {
			alert('Please, submit required data');
			document.form1.sku.focus();				
			return event.preventDefault();
		}

		if (document.form1.name.value == '') {
			alert('Please, submit required data');
			document.form1.name.focus();
			return event.preventDefault();
		}
        if (document.form1.price.value == '') {
			alert('Please, submit required data');
			document.form1.price.focus();				
			return event.preventDefault();
		} 
        
        console.log(disc_input_check);
        
        let dynamic_res = ""

        if(furni_display_check == '') {
            dynamic_res = "furniture"; } 

        else if (book_display_check == '') {
            dynamic_res = "book";} 
       
        else if (disc_display_check == '') {
            dynamic_res = "disc"
        }

        else {
            alert('Please, submit required data');
			document.form1.type_switcher1.focus();				
			return event.preventDefault();
        }

        switch (dynamic_res) {
            case "disc": 
                if (disc_input_check == "") {
                alert('Please, submit required data');
			    document.form1.size.focus();				
			    return event.preventDefault(); }
                break;

            case "book": 
                if (book_input_check == "") {
                alert('Please, submit required data');
			    document.form1.weight.focus();				
			    return event.preventDefault();
                }
                break;

            case "furniture": 
                
                if (furni_input_check_ht == "" || furni_input_check_wd == "" || furni_input_check_lt == "") {
                    if (document.form1.height.value == '') {
                        alert('Please, submit required data');
                        document.form1.height.focus();				
                        return event.preventDefault();
                    }  
                    if (document.form1.width.value == '') {
                        alert('Please, submit required data');
                        document.form1.width.focus();				
                        return event.preventDefault();
                    }  
                    if (document.form1.length.value == '') {
                        alert('Please, submit required data');
                        document.form1.length.focus();				
                        return event.preventDefault();
                    } 
                    break;
                }
    
            default: return true; 
        }
		
	}
</script>	

</body>
</html>